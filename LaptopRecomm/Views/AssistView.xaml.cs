﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.ViewModels;

namespace WpfApp1.Views
{
    /// <summary>
    /// Interaction logic for AssistVIew.xaml
    /// </summary>
    public partial class AssistView : UserControl
    {
        private List<KeyValuePair<string, string>> imageKVPList = new List<KeyValuePair<string, string>>();
        private List<LaptopModel> laptopList = new List<LaptopModel>();
        private int questionCount = 0;

        private string[] engageText = {
            "We would like to know more about you.",
            "Laptops shown will update according you your needs.",
            "We are working on these list of laptops.",
            "Hang in there just few more questions.",
            "These few laptops should fit your needs.",
            "We updated to these choices.",
            "These are just perfect for you!"
        };

        public AssistView()
        {
            InitializeComponent();
            GetLaptopImages();
            questionCount = -1; // ignore init refresh
        }

        public void Refresh(List<LaptopModel> laptopList)
        {
            this.laptopList.Clear();
            this.laptopList.AddRange(laptopList);
            if (imageKVPList.Count > 0)
            {
                PopulateImages();
            }

            // controlling the suggestion message

            if (laptopList.Count <= 3)
            {
                // conclusion hit
                this.SuggestionTB.Text = engageText[engageText.Length - 1];
            }
            else
            {
                // show next suggestion
                if (questionCount >= 0)
                {
                    this.SuggestionTB.Text = engageText[questionCount % (engageText.Length - 2)];
                }
            }
            questionCount++;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void GetLaptopImages()
        {
            this.imageKVPList.Clear();
            var curDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string imageFolder = System.IO.Path.Combine(curDir, @"Images");

            DirectoryInfo dirInfo = new DirectoryInfo(imageFolder);
            FileInfo[] info = dirInfo.GetFiles("*.*");
            foreach (FileInfo f in info)
            {
                string[] nameSplit = f.Name.Split('_');
                string fullPath = string.Format("{0}\\{1}", f.DirectoryName, f.Name);
                var kvp = new KeyValuePair<string, string>(nameSplit[0], fullPath);
                imageKVPList.Add(kvp);
            }
        }

        private void PopulateImages()
        {
            this.ImageWrapPanel.Children.Clear();
            this.ImageWrapPanel.Children.Capacity = 16;
            laptopList.Sort((a, b) => a.Cf.CompareTo(b.Cf));
            laptopList.Reverse();

            for (int i = 0; i < 15; i++)
            {
                if (i >= laptopList.Count)
                {
                    break;
                }
                string id = laptopList[i].Id;
                var imageKvp = imageKVPList.FirstOrDefault(kvp => kvp.Key.Equals(id, StringComparison.CurrentCultureIgnoreCase));

                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.UriSource = new Uri(imageKvp.Value, UriKind.RelativeOrAbsolute);
                bitmapImage.EndInit();

                Image image = new Image
                {
                    Width = 130,
                    Height = 100,
                    Margin = new Thickness(10),
                    Source = bitmapImage,
                    ToolTip = string.Format("{0} - cf{1}", laptopList[i].Name, Math.Round(laptopList[i].Cf, 6)),
                    //ToolTip = string.Format("{0}", laptopList[i].Name),
                    Tag = "http://h20386.www2.hp.com/SingaporeStore/Merch/List.aspx?sel=NTB&ctrl=f"
                };
                if (!laptopList[i].Url.Equals("Not available", StringComparison.CurrentCultureIgnoreCase))
                {
                    image.Tag = string.Format("http://h20386.www2.hp.com/SingaporeStore/Merch/Product.aspx?id={0}&opt=&sel=NTB", laptopList[i].Url);
                }
                image.MouseDown += Image_MouseDown;

                this.ImageWrapPanel.Children.Add(image);
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            string url = (sender as Image).Tag as string;
            if (!string.IsNullOrEmpty(url))
            {
                System.Diagnostics.Process.Start(url);
            }
        }
    }
}
