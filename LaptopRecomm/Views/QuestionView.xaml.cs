﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.ViewModels;

namespace WpfApp1.Views
{
    /// <summary>
    /// Interaction logic for Question1.xaml
    /// </summary>
    public partial class QuestionView : UserControl
    {
        public QuestionView()
        {
            InitializeComponent();
            this.DataContext = new QuestionVM();
            this.Unloaded += Question_Unloaded;
        }

        private void Question_Unloaded(object sender, RoutedEventArgs e)
        {
            // Exiting question
            ItemCollection itemCollection = RadioBtnLB.Items;
        }

        private void RadioBtnLB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            (this.DataContext as QuestionVM).SelectedAnswer = e.AddedItems[0].ToString();
        }

    }
}
