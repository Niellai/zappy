﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.ViewModels;

namespace WpfApp1.Views
{
    /// <summary>
    /// Interaction logic for Result.xaml
    /// </summary>
    public partial class ResultView : UserControl
    {

        private List<KeyValuePair<string, string>> imageKVPList = new List<KeyValuePair<string, string>>();

        public ResultView()
        {
            InitializeComponent();
            this.DataContext = new ResultVM();
            GetLaptopImages();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ResultVM resultVM = (this.DataContext as ResultVM);
            this.RefNum.Text = Guid.NewGuid().ToString();
            int count = 0;
            foreach (LaptopModel model in resultVM.LaptopList)
            {
                var kvp = imageKVPList.FirstOrDefault(i => i.Key.Equals(model.Id, StringComparison.CurrentCultureIgnoreCase));
                switch (count)
                {
                    case 0:
                        image1.Source = new BitmapImage(new Uri(kvp.Value, UriKind.RelativeOrAbsolute));
                        image1.ToolTip = new ToolTip().Content = model.Name;
                        if (!model.Url.Equals("Not available", StringComparison.CurrentCultureIgnoreCase))
                        {
                            image1.Tag = string.Format("http://h20386.www2.hp.com/SingaporeStore/Merch/Product.aspx?id={0}&opt=&sel=NTB", model.Url);
                        }
                        else
                        {
                            image1.Tag = "http://h20386.www2.hp.com/SingaporeStore/Merch/List.aspx?sel=NTB&ctrl=f";
                        }
                        image1.MouseDown += Image_MouseDown;
                        break;
                    case 1:
                        image2.Source = new BitmapImage(new Uri(kvp.Value, UriKind.RelativeOrAbsolute));
                        image2.ToolTip = new ToolTip().Content = model.Name;
                        if (!model.Url.Equals("Not available", StringComparison.CurrentCultureIgnoreCase))
                        {
                            image2.Tag = string.Format("http://h20386.www2.hp.com/SingaporeStore/Merch/Product.aspx?id={0}&opt=&sel=NTB", model.Url);
                        }
                        else
                        {
                            image2.Tag = "http://h20386.www2.hp.com/SingaporeStore/Merch/List.aspx?sel=NTB&ctrl=f";
                        }
                        image2.MouseDown += Image_MouseDown;
                        break;
                    case 2:
                        image3.Source = new BitmapImage(new Uri(kvp.Value, UriKind.RelativeOrAbsolute));
                        image3.ToolTip = new ToolTip().Content = model.Name;
                        if (!model.Url.Equals("Not available", StringComparison.CurrentCultureIgnoreCase))
                        {
                            image3.Tag = string.Format("http://h20386.www2.hp.com/SingaporeStore/Merch/Product.aspx?id={0}&opt=&sel=NTB", model.Url);
                        }
                        else
                        {
                            image3.Tag = "http://h20386.www2.hp.com/SingaporeStore/Merch/List.aspx?sel=NTB&ctrl=f";
                        }
                        image3.MouseDown += Image_MouseDown;
                        break;
                }

                count++;
                if (count > 3)
                {
                    break;
                }
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            string url = (sender as Image).Tag as string;
            if (!string.IsNullOrEmpty(url))
            {
                System.Diagnostics.Process.Start(url);
            }
        }

        private void GetLaptopImages()
        {
            var curDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string imageFolder = System.IO.Path.Combine(curDir, @"Images");

            DirectoryInfo dirInfo = new DirectoryInfo(imageFolder);
            FileInfo[] info = dirInfo.GetFiles("*.*");
            foreach (FileInfo f in info)
            {
                string[] nameSplit = f.Name.Split('_');
                string fullPath = string.Format("{0}\\{1}", f.DirectoryName, f.Name);
                var kvp = new KeyValuePair<string, string>(nameSplit[0], fullPath);
                imageKVPList.Add(kvp);
            }
        }
    }
}
