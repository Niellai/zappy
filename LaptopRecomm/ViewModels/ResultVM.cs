﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.ViewModels
{
    public class ResultVM
    {

        public void SetLaptopList(List<LaptopModel> resultList)
        {
            this.LaptopList = new ObservableCollection<LaptopModel>(resultList);
        }

        private ObservableCollection<LaptopModel> _laptopList;
        public ObservableCollection<LaptopModel> LaptopList
        {
            get { return _laptopList; }
            set { _laptopList = value; }
        }

    }
}
