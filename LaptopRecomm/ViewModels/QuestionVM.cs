﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.ViewModels
{
    public class QuestionVM
    {

        public QuestionVM()
        {
            this.SelectedIndex = -1;
        }

        public int SelectedIndex { get; set; }

        private string _selectedAnswer;
        public string SelectedAnswer
        {
            get { return _selectedAnswer; }
            set
            {
                this.SelectedIndex = 0;
                _selectedAnswer = value;
                for (int i = 0; i < _answers.Count; i++)
                {
                    if (_answers[i].Equals(_selectedAnswer, StringComparison.CurrentCultureIgnoreCase))
                    {
                        this.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        private string _question;
        public string Question
        {
            get { return _question; }
            set { _question = value; }
        }

        private ObservableCollection<string> _answers;
        public ObservableCollection<string> Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }

    }
}
