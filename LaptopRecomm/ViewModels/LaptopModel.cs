﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.ViewModels
{
    public class LaptopModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double Cf { get; set; }
        public double Rank { get; set; }
        public string Url { get; set; }
        //public string Weight { get; set; }
        //public string Screen_size { get; set; }
        //public string Form_factor { get; set; }
        //public string Cpu { get; set; }
        //public string Battery { get; set; }
        //public string Fast_charge { get; set; }
        //public string Good_audio { get; set; }
        //public string Gpu { get; set; }
        //public string Ram { get; set; }
        //public string Price { get; set; }
        //public string Hdd_type { get; set; }
    }
}
