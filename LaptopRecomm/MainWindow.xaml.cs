﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.ViewModels;
using WpfApp1.Views;
using System.Reflection;
using System.IO;
using Mommosoft.ExpertSystem;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private Mommosoft.ExpertSystem.Environment clips = new Mommosoft.ExpertSystem.Environment();
        private Mommosoft.ExpertSystem.Environment clips = new Mommosoft.ExpertSystem.Environment();
        private ArrayList questionList;
        private int curControlIndex = 0;
        private List<UserControl> controlList = new List<UserControl>();
        private List<LaptopModel> laptopList = new List<LaptopModel>();
        private int mCurQns = 0; //Storing current question Index
        private int max_question = 7;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // init clips files
            try
            {
                //Test to show conclusion
                //this.MainContent.Content = new ResultView();

                // Init the assist view 
                this.AssistContent.Content = new AssistView();

                var curDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string clpFolder = System.IO.Path.Combine(curDir, @"Clips");

                // Locating clips folder
                if (System.IO.Directory.Exists(clpFolder))
                {
                    string templatesClpFile = String.Format("{0}\\templates.clp", clpFolder);
                    string engineClpFile = String.Format("{0}\\engine.clp", clpFolder);
                    string productClpFile = String.Format("{0}\\product.clp", clpFolder);
                    List<string> clpList = new List<string>();

                    clpList.Add(templatesClpFile);
                    clpList.Add(productClpFile);
                    clpList.Add(engineClpFile);

                    foreach (string file in clpList)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            clips.Load(file);
                        }
                    }
                    clips.Reset();
                }

                GetProductList();
                this.questionList = GetAllQuestionsRule();
                this.progressBar.Maximum = max_question;
                NextQuestions();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        #region UI Component

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            curControlIndex--;
            if (curControlIndex < 0)
            {
                curControlIndex = 0;
            }
            UpdateControl();
        }

        private void NextBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.MainContent.Content is ResultView)
            {
                // reset the program
                this.AssistContent.Content = new AssistView();
                mCurQns = 0;
                curControlIndex = 0;
                controlList.Clear();
                clips.Reset();
                this.progressBar.Value = 0;
                NextQuestions();
                this.NextBtnIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.StepForward;
            }
            else
            {
                QuestionView qnsControl = controlList[curControlIndex] as QuestionView;
                int selectedIndex = (qnsControl.DataContext as QuestionVM).SelectedIndex;

                if (selectedIndex >= 0)
                {
                    curControlIndex++;
                    SubmitAns(this.mCurQns, selectedIndex);
                }
            }
        }

        private void UpdateControl()
        {
            this.BackBtn.Visibility = Visibility.Collapsed;
            this.NextBtn.Visibility = Visibility.Visible;

            this.MainContent.Content = controlList[curControlIndex];
            if (this.MainContent.Content is ResultView)
            {
                this.progressBar.Value = max_question;
                this.NextBtnIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Refresh;
            }
            else
            {
                this.progressBar.Value = curControlIndex + 1;
            }
        }

        private void UpdateAssistControl(List<LaptopModel> conclusionList = null)
        {
            if (conclusionList == null)
            {
                // conculsion is not found show top 15 list of laptop
                GetProductList();
                (this.AssistContent.Content as AssistView).Refresh(this.laptopList);
            }
            else
            {
                // conculsion is found show only top 3
                (this.AssistContent.Content as AssistView).Refresh(conclusionList);
            }
        }
        #endregion


        #region Clips Component

        private void SubmitAns(int curQns, int curAns)
        {
            string assertCmd = string.Format("(cur_question (cur_qns {0}) (cur_ans {1}))", curQns, curAns);
            AssertRules(assertCmd);

            List<LaptopModel> laptopModelList = new List<LaptopModel>();
            string evalStr = "(get_next_question)";
            MultifieldValue mv = (MultifieldValue)clips.Eval(evalStr);
            for (int i = 0; i < mv.Count; i++)
            {
                FactAddressValue fv = (FactAddressValue)mv[i];

                string qns_id = fv.GetFactSlot("next_qns").ToString().Replace("\"", "");
                Int16 tryQnsId = -1;
                Int16.TryParse(qns_id, out tryQnsId);
                if (tryQnsId != -1)
                {
                    this.mCurQns = tryQnsId;
                }

                // try to see if reach conclusion
                List<LaptopModel> resultList = TryGetConclusion();
                if (resultList.Count > 0)
                {
                    ShowConclusion(resultList);
                    UpdateAssistControl(resultList);
                }
                else
                {
                    // no conclusion ask next question
                    NextQuestions();
                }
            }

        }

        /// <summary>
        /// <para>
        /// Assert rules into clips
        /// </para>        
        /// <para> Eg.(current_fact (fact human) (cf 0.5))</para>
        /// </summary>
        /// <param name="clipStr"></param>
        private void AssertRules(string assertCmd)
        {
            clips.AssertString(assertCmd);
            clips.Run();
        }

        private void GetCurrentGoal()
        {
            string debugMsg = "";
            DebugTB.Text = debugMsg;
            ArrayList questionList = new ArrayList();
            string evalStr = "(get_current_goal)";
            MultifieldValue mv = (MultifieldValue)clips.Eval(evalStr);
            for (int i = 0; i < mv.Count; i++)
            {
                FactAddressValue fv = (FactAddressValue)mv[i];
                string factor = fv.GetFactSlot("factor").ToString().Replace("\"", "");
                string value = fv.GetFactSlot("value").ToString().Replace("\"", "");
                debugMsg += string.Format("{0}: {1}, ", factor, value);
            }
            DebugTB.Text = debugMsg;
        }

        /// <summary>
        /// <para>
        ///     Retreive all questions from clips
        /// </para>       
        /// </summary>       
        private ArrayList GetAllQuestionsRule()
        {
            ArrayList questionList = new ArrayList();
            string evalStr = "(get_all_questions)";
            MultifieldValue mv = (MultifieldValue)clips.Eval(evalStr);
            for (int i = 0; i < mv.Count; i++)
            {
                FactAddressValue fv = (FactAddressValue)mv[i];
                string question = fv.GetFactSlot("qns").ToString().Replace("\"", "");
                questionList.Add(question);
            }
            return questionList;
        }

        /// <summary>
        /// <para>
        ///     Retreive all answer for indvidual question number
        /// </para>
        /// </summary>
        /// <param name="qnNum"></param>
        private List<string> GetAnswersRule(int qnNum)
        {
            List<string> answerList = new List<string>();
            string evalStr = string.Format("(get_answer {0})", qnNum);
            MultifieldValue mv = (MultifieldValue)clips.Eval(evalStr);
            for (int i = 0; i < mv.Count; i++)
            {
                FactAddressValue fv = (FactAddressValue)mv[i];
                MultifieldValue multiFieldValue = (MultifieldValue)fv.GetFactSlot("ans");
                foreach (PrimitiveValue value in multiFieldValue)
                {
                    string answer = value.ToString().Replace("\"", "");
                    answerList.Add(answer);
                }
            }
            return answerList;
        }

        private List<LaptopModel> TryGetConclusion()
        {
            List<LaptopModel> resultList = new List<LaptopModel>();
            string evalStr = "(get_conculsion)";
            MultifieldValue mv = clips.Eval(evalStr) as MultifieldValue;
            for (int i = 0; i < mv.Count; i++)
            {
                FactAddressValue fv = (FactAddressValue)mv[i];
                string laptopId = fv.GetFactSlot("laptop").ToString();
                string laptopRank = fv.GetFactSlot("rank").ToString();
                string laptopCf = fv.GetFactSlot("cf").ToString();

                LaptopModel model = FindProductById(laptopId);
                if (model != null)
                {
                    model.Rank = double.Parse(laptopRank);
                    model.Cf = double.Parse(laptopCf);
                    resultList.Add(model);
                }
            }
            return resultList;
        }

        /// <summary>
        /// Retrieve all products from clips and store it in LaptopList
        /// </summary>
        private void GetProductList()
        {
            this.laptopList.Clear();
            string evalStr = "(get_products)";
            MultifieldValue mv = clips.Eval(evalStr) as MultifieldValue;
            for (int i = 0; i < mv.Count; i++)
            {
                FactAddressValue fv = (FactAddressValue)mv[i];
                string id = fv.GetFactSlot("id").ToString().Replace("\"", "");
                string name = fv.GetFactSlot("name").ToString().Replace("\"", "");
                string cf = fv.GetFactSlot("cf").ToString().Replace("\"", "");
                string code = fv.GetFactSlot("code").ToString().Replace("\"", "");

                LaptopModel model = new LaptopModel()
                {
                    Id = id,
                    Name = name,
                    Cf = double.Parse(cf),
                    Url = code

                };
                this.laptopList.Add(model);
            }
        }

        private LaptopModel FindProductById(string id)
        {
            return this.laptopList.FirstOrDefault(i => i.Id.Equals(id, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Create question views and add it to controlList
        /// </summary>
        private void NextQuestions()
        {
            QuestionView questionView = new QuestionView();
            QuestionVM questionVM = questionView.DataContext as QuestionVM;
            questionVM.Question = questionList[this.mCurQns].ToString();
            questionVM.Answers = new ObservableCollection<string>(GetAnswersRule(this.mCurQns));
            controlList.Add(questionView);

            this.progressBar.Value = this.mCurQns;
            UpdateControl();
            UpdateAssistControl();
            GetCurrentGoal();
        }

        private void ShowConclusion(List<LaptopModel> resultList)
        {
            ResultView resultView = new ResultView();
            ResultVM resultVM = resultView.DataContext as ResultVM;
            resultVM.SetLaptopList(resultList);
            controlList.Add(resultView);
            UpdateControl();
        }
        #endregion

    }
}
