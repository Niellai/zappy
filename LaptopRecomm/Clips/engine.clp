;; Low Medium High
; 0.1 ~ 0.4 Low
; 0.4 ~ 0.7 Medium
; 0.7 ~ 1 High
; anything that is negative is not recommed to users
;
; Yes No
; 0 ~ 0.5 Yes
; 0.5 ~ 1 No
;;

(deffacts cf_factor_weight
  (factor_weight (factor weight) (weight 0.6))
  (factor_weight (factor screen_size) (weight 0.6))
  (factor_weight (factor form_factor) (weight 0.7))
  (factor_weight (factor cpu) (weight 0.95))
  (factor_weight (factor battery) (weight 0.6))
  (factor_weight (factor fast_charge) (weight 0.4))
  (factor_weight (factor good_audio) (weight 0.3))
  (factor_weight (factor gpu) (weight 0.8))
  (factor_weight (factor ram) (weight 0.8))
  (factor_weight (factor price) (weight 0.7))
  (factor_weight (factor hdd_type) (weight 0.7))
)

;; load Facts
(deffacts load-goal "Current Goal"
	(current_goal (factor weight) (value 0.55))
  (current_goal (factor screen_size) (value 0.55))
  (current_goal (factor form_factor) (value 0.5))
  (current_goal (factor cpu) (value 0.55))
  (current_goal (factor battery) (value 0.55))
  (current_goal (factor fast_charge) (value 0.5))
  (current_goal (factor good_audio) (value 0.5))
  (current_goal (factor gpu) (value 0.5))
  (current_goal (factor ram) (value 0.55))
  (current_goal (factor price) (value 0.55))
  (current_goal (factor hdd_type) (value 0.55))
)

(deffacts load-factor_hit "Current Goal"
  (total_factor_hit (count 0))
	(factor_hit (factor weight) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor screen_size) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor form_factor) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor cpu) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor battery) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor fast_charge) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor good_audio) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor gpu) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor ram) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor price) (count 0) (add_to_total 0) (eval 0))
  (factor_hit (factor hdd_type) (count 0) (add_to_total 0) (eval 0))
)

(deffacts load-question_count "Current Goal"
	(question_count (count 0))
)

(deffacts load-qna
  (next_question (next_qns 0))
  (question (id 0) (qns "What is the purpose for buying a laptop?"))
  (question (id 1) (qns "Which group do you belong to?"))
  (question (id 2) (qns "Which major subject are you studying currently?"))
  (question (id 3) (qns "What is your occupation?"))
  (question (id 4) (qns "What program do you use on a regular basis?"))
  (question (id 5) (qns "Choose the type of game that you play the most."))
  (question (id 6) (qns "Pick the functionality that you most prefer."))
  (question (id 7) (qns "Do you watch movie or listen to music often on your laptop?"))
  (question (id 8) (qns "Do you have the habit of running multi programs on your laptop?"))
  (question (id 9) (qns "How often do you change your laptop?"))
  (question (id 10) (qns "How often do you bring your laptop outside of office or home?"))
  (question (id 11) (qns "How many tabs do you have when using web broswer?"))
  (question (id 12) (qns "Are you required to perform simulation/ modeling in your work/ study?"))
  (question (id 13) (qns "How old are you currently?"))

  (answer (id 0)
          (ans_id 0 1)
          (ans
            "Personal"
            "School/Work"))
  (answer (id 1)
          (ans_id 0 1 2)
          (ans
            "Secondary School Student"
            "Polytechnical/ Undergrad/ Postgrad Student"
            "Working Professional"))
  (answer (id 2)
          (ans_id 0 1 2 3 4 5)
          (ans
            "Business (Including Law, Economics)"
            "Science (Including Medicine, Pharmacy)"
            "Arts, Humanities & Social Science (Including Music)"
            "Design and Environment"
            "Engineering"
            "Information Technology (Including Computering)"))
  (answer (id 3)
          (ans_id 0 1 2 3 4 5)
          (ans
            "Legislators/ Senior Officials/ Managers and Professionals"
            "Associate Professionals/ Technicians"
            "Clerical Support/ Service and Sales personal"
            "Agricultural/ Fishery/ Craftsmen and Related Trades"
            "Plant and Machine Operators/ Assemblers"
            "Others"))
  (answer (id 4)
          (ans_id 0 1 2 3 4 5 6)
          (ans
            "Computer-aided Design Program (Autocad, Microstation)"
            "Scientific (Mathlab, Mathematica)"
            "Graphics Programs (Abode Cloud Services - Photoshop)"
            "Games (Call of Duty, Warhammer, FF XIV)"
            "Video Streaming"
            "Microsoft Office (Excel, Words, Outlook)"
            "None of the above"
            ))
  (answer (id 5)
          (ans_id 0 1 2 3 4)
          (ans
            "MMORPG (World of Warcraft)"
            "eSports titles (Dota2)"
            "RPG (Witcher 3)"
            "Simulation (Sims 4)"
            "Light online games (Maplestory)"))
  (answer (id 6)
          (ans_id 0 1)
          (ans
            "Portability"
            "Performance"))
  (answer (id 7)
          (ans_id 0 1)
          (ans
            "Yes"
            "No"))
  (answer (id 8)
          (ans_id 0 1)
          (ans
            "Yes"
            "No"))
  (answer (id 9)
          (ans_id 0 1 2 3)
          (ans
            "Yearly"
            "Every 3 years"
            "Every 5 years"
            "Only when laptop is no longer functioning"))
  (answer (id 10)
          (ans_id 0 1 2)
          (ans
            "Everyday"
            "Every week"
            "Once a month"))
  (answer (id 11)
          (ans_id 0 1)
          (ans
            "1 to 10"
            "11 to 30"
            ">30"))
  (answer (id 12)
          (ans_id 0 1)
          (ans
            "Yes"
            "No"))
  (answer (id 13)
          (ans_id 0 1 2 3)
          (ans
            "<30"
            "30-50"
            "50-60"
            ">60"))
)

;; combine questions
(defrule combine-goal
  ?f1 <- (current_goal (factor ?factor1) (value ?value1&:(>= ?value1 0)))
  ?f2 <- (working_goal (factor ?factor1) (value ?value2&:(>= ?value2 0)))
  (test (neq ?f1 ?f2)) ; test pointers and not value
  =>
  (retract ?f2)
  (modify ?f1 (value =(+ ?value1 (* ?value2 (- 1 ?value1)))))
)

;; retrieve all questions
(deffunction get_all_questions()
  (bind ?questions
    (find-all-facts ((?q question)) TRUE))
)

;; retrieve all answers
(deffunction get_all_answers()
  (bind ?answers
    (find-all-facts ((?a answer)) TRUE))
)

;; pass question id to get respective answers for that question
(deffunction get_answer(?qns_id)
  (bind ?answers
    (find-all-facts ((?a answer))
                    (eq ?a:id ?qns_id)))
)

(deffunction get_products()
  (find-all-facts ((?a product)) TRUE)
)

(deffunction get_next_question()
  (find-all-facts ((?a next_question)) TRUE)
)

(deffunction get_conculsion()
  (find-all-facts ((?a conclusion)) TRUE)
)

;; questionn function to create working goal for each factor hit
(deffunction qn_factor_goal (?factor_hit ?factor ?count ?add_total ?value)
  (if (eq ?count 0) then
    (modify ?factor_hit (add_to_total (+ ?add_total 1)) (eval 1) (count (+ ?count 1)))
    else
    (modify ?factor_hit (eval 1) (count (+ ?count 1)))
  )
  (assert (working_goal (factor ?factor) (value ?value)))
)

(deffunction get_current_goal ()
  (find-all-facts ((?a current_goal)) TRUE)
)

;; get sum of factor hits
(defrule get_total_factor_hits
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (add_to_total ?add_total&:(eq ?add_total 1))
  )
  ?factor_total <- (total_factor_hit (count ?total_count))
  (test (neq ?factor total)) ; test pointers and not value
  =>
  (modify ?factor_total (count (+ ?total_count 1)))
  (modify ?factor_hit (add_to_total 0))
)

;; question function to determine next question if
;; questions
(defrule check_question_answered
  ?cur_qn <- (cur_question (cur_qns ?qn) (cur_ans ?ans))
  ?qn_count <- (question_count (answer $?answer) (question $?question))
  (test (not (member$ ?qn $?question)))
  =>
  (modify
    ?qn_count
    (question (insert$ $?question (+ (length$ $?question) 1) ?qn))
    (answer (insert$ $?answer (+ (length$ $?answer) 1) ?ans))
  )
)
;; question_0 does nothing, all questions are define here
(defrule question_1
  ?qns <- (cur_question (cur_qns 1) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case fast_charge then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (case 1 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case fast_charge then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (case 2 then
      (switch ?factor
        (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case fast_charge then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
      )
  )
)

(defrule question_2
  ?qns <- (cur_question (cur_qns 2) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.55))
      )
    )
    (case 1 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.55))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (case 2 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (default then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
  )
)

(defrule question_3
  ?qns <- (cur_question (cur_qns 3) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (case 1 then
      (switch ?factor
        (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (default then
      (switch ?factor
        (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
  )
)

(defrule question_4
  ?qns <- (cur_question (cur_qns 4) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case form_factor then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (case 1 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (case 2 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (case 3 then
      (switch ?factor
        (case form_factor then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case good_audio then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (case 4 then
      (switch ?factor
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.3))
      )
    )
    (case 5 then
      (switch ?factor
        (case form_factor then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
  )
)

(defrule question_5
  ?qns <- (cur_question (cur_qns 5) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 2 then
      (switch ?factor
        (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case good_audio then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (case 4 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case good_audio then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (default then
      (switch ?factor
        (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case good_audio then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
  )
)

(defrule question_6
  ?qns <- (cur_question (cur_qns 6) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total -0.4))
        (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total -0.4))
        ; (case form_factor then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        ; (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        ; (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.2))
        ; (case fast_charge then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        ; (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        ; (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        ; (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        ; (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.2))
      )
    )
    (case 1 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.3))
        ; (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        ; (case form_factor then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.3))
        ; (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        ; (case fast_charge then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
        (case gpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.3))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.2))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.3))
        ; (case price then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
  )
)

(defrule question_7
  ?qns <- (cur_question (cur_qns 7) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case good_audio then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
      )
    )
    (case 1 then
      (switch ?factor
        (case good_audio then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0))
      )
    )
  )
)

(defrule question_8
  ?qns <- (cur_question (cur_qns 8) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (case 1 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
  )
)

(defrule question_9
  ?qns <- (cur_question (cur_qns 9) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (case 1 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
    (default then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
      )
    )
  )
)

(defrule question_10
  ?qns <- (cur_question (cur_qns 10) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.1))
        (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case fast_charge then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
      )
    )
    (case 1 then
      (switch ?factor
        (case weight then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case battery then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
  )
)

(defrule question_11
  ?qns <- (cur_question (cur_qns 11) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
      )
    )
    (case 1 then
      (switch ?factor
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.55))
      )
    )
    (case 2 then
      (switch ?factor
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.85))
      )
    )
  )
)

(defrule question_12
  ?qns <- (cur_question (cur_qns 12) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 0 then
      (switch ?factor
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case hdd_type then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
        (case ram then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.7))
      )
    )
  )
)

(defrule question_13
  ?qns <- (cur_question (cur_qns 13) (cur_ans ?ans))
  ?factor_hit <- (factor_hit
    (factor ?factor)
    (count ?count)
    (eval ?eval&:(eq ?eval 0))
    (add_to_total ?add_total)
  )
  =>
  (switch ?ans
    (case 3 then
      (switch ?factor
        (case screen_size then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
        (case cpu then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 0.4))
        (case good_audio then (qn_factor_goal ?factor_hit ?factor ?count ?add_total 1))
      )
    )
  )
)

;; Match laptop's factor to current_goal, matched factor will be marked with 1
(defrule uneval_product_factor
  (declare (salience -10))
  ?cur_qn <- (cur_question)
  ?product <- (product (id ?id) (eval 2))
  ?product_factor <- (product_factor (id ?id) (eval 2))
  =>
  (modify ?product_factor (eval 0))
)
(defrule uneval_product
  (declare (salience -20))
  ?cur_qn <- (cur_question)
  ?product <- (product (eval 2))
  =>
  (modify ?product (eval 0))
)

(defrule match
  (declare (salience -30))
  ?cur_goal <- (current_goal (factor ?factor) (value ?value))
  ?product_factor <- (product_factor (factor ?factor) (match ?match) (min ?min) (max ?max) (eval 0))
  =>
  (if (and (>= ?value ?min) (<= ?value ?max)) then
    (modify ?product_factor (eval 1) (match 1))
    else
    (modify ?product_factor (eval 1) (match -1))
  )
)

;; compilation rule, calculate the total cf use rank laptop for recommentation
(defrule compilation
  (declare (salience -40))
  ?rank <- (factor_weight (factor ?factor) (weight ?weight))
  ?product_factor <- (product_factor (id ?id) (factor ?factor) (match ?match) (eval 1))
  ?product <- (product (id ?id))
  =>
  (assert (product_working_cf (id ?id) (cf (* ?weight ?match))))
  (modify ?product_factor (eval 2))
)
(defrule combine-compile-positive-cf
  (declare (salience -35))
  ?f1 <- (product (id ?id)(cf ?cf1&:(>= ?cf1 0)))
  ?f2 <- (product_working_cf (id ?id)(cf ?cf2&:(>= ?cf2 0)))
  (test (neq ?f1 ?f2)) ; test pointers and not value
  =>
  (retract ?f2)
  (modify ?f1 (cf =(+ ?cf1 (* ?cf2 (- 1 ?cf1)))))
)

;combine NEGATIVE cf
;cf(cf1,cf2) = cf1 + cf2 * (1 + cf1)

(defrule combine-compile-neg-cf
 (declare (salience -36))
  ?f1 <- (product (id ?id)(cf ?cf1&:(< ?cf1 0)))
  ?f2 <- (product_working_cf (id ?id)(cf ?cf2&:(< ?cf2 0)))
  (test (neq ?f1 ?f2))
  =>
  (retract ?f2)
  (modify ?f1 (cf =(+ ?cf1 (* ?cf2 (+ 1 ?cf1)))))
)

;combine one POSITIVE and one NEGATIVE
;cf(cf1,cf2) = (cf1 + cf2) / 1 - MIN[abs(cf1),abs(cf2)]

(defrule combine-compile-neg-pos-cf
 (declare (salience -36))
  ?f1 <- (product (id ?id) (cf ?cf1))
  ?f2 <- (product_working_cf (id ?id) (cf ?cf2))
  (test (neq ?f1 ?f2))
  (test (<= (* ?cf1 ?cf2) 0))
  =>
  (retract ?f2)
  (modify ?f1 (cf =(/ (+ ?cf1 ?cf2) (- 1 (min (abs ?cf1) (abs ?cf2))))))
)


;; question flow logic, program will assert cur_question and triggers rule to fire
(defrule choose_next_question
  (declare (salience -50))
  ?qns <- (cur_question (cur_qns ?qid) (cur_ans ?ans))
  ?nqns <- (next_question (next_qns ?nqid))
  ?qn_count <- (question_count (count ?count))
  =>
  (modify ?qn_count (count (+ ?count 1)))
  (switch ?qid
    (case 0 then
      (if (eq ?ans 0) then
        (modify ?nqns (next_qns 4))
      else
        (modify ?nqns (next_qns 1))
      )
    )
    (case 1 then
      (if (eq ?ans 0) then
        (modify ?nqns (next_qns "L"))
      else
        (if (eq ?ans 1) then
          (modify ?nqns (next_qns 2))
        else
          (modify ?nqns (next_qns 3))
        )
      )
    )
    (case 2 then
      (if (or (eq ?ans 3) (eq ?ans 4) (eq ?ans 5)) then
        (modify ?nqns (next_qns 12))
        else
        (modify ?nqns (next_qns "L"))
      )
    )
    (case 3 then
      (modify ?nqns (next_qns "L"))
    )
    (case 4 then
      (if (eq ?ans 3) then
        (modify ?nqns (next_qns 5))
      else
        (modify ?nqns (next_qns "L"))
      )
    )
    (case 5 then
      (modify ?nqns (next_qns "L"))
    )
    (case 6 then
      (modify ?nqns (next_qns "L"))
    )
    (case 7 then
      (modify ?nqns (next_qns "L"))
    )
    (case 8 then
      (modify ?nqns (next_qns "L"))
    )
    (case 9 then
      (modify ?nqns (next_qns "L"))
    )
    (case 10 then
      (modify ?nqns (next_qns "L"))
    )
    (case 11 then
      (modify ?nqns (next_qns "L"))
    )
    (case 12 then
      (modify ?nqns (next_qns "L"))
    )
    (case 13 then
      (modify ?nqns (next_qns "L"))
    )
  )
  (retract ?qns)
)


;; conclusion and question L handling
(defrule question_L
  (declare (salience -60))
  ?qns_count <- (question_count (count ?count_q) (question $?question))
  ?total_hit <- (total_factor_hit (count ?count_f))
  ?weight <- (factor_hit (factor weight) (count ?count_weight))
  ?cpu <- (factor_hit (factor cpu) (count ?count_cpu))
  ?hdd <- (factor_hit (factor hdd_type) (count ?count_hdd))
  ?battery <- (factor_hit (factor battery) (count ?count_battery))
  ?fast <- (factor_hit (factor fast_charge) (count ?count_fast))
  ?ram <- (factor_hit (factor ram) (count ?count_ram))
  ?audio <- (factor_hit (factor good_audio) (count ?count_audio))
  ?nqns <- (next_question (next_qns "L"))
  =>
  (if (or (>= ?count_q 6) (>= ?count_f 10)) then
    (modify ?nqns (next_qns "C"))
    else
    (if (and (< ?count_f 5) (not (member$ 13 $?question))) then
      (modify ?nqns (next_qns 13))
      else
      (if (and (eq ?count_ram 0) (not (member$ 11 $?question))) then
        (modify ?nqns (next_qns 11))
        else
        (if (and (eq ?count_cpu 0) (not (member$ 9 $?question))) then
          (modify ?nqns (next_qns 9))
          else
          (if (and (or (eq ?count_weight 0) (eq ?count_fast 0)) (not (member$ 10 $?question))) then
            (modify ?nqns (next_qns 10))
            else
            (if (and (eq ?count_audio 0) (not (member$ 7 $?question))) then
              (modify ?nqns (next_qns 7))
              else
              (if (and (or (eq ?count_battery 0) (eq ?count_hdd 0) (eq ?count_ram 0) (eq ?count_cpu 0)) (not (member$ 8 $?question))) then
                (modify ?nqns (next_qns 8))
                else
                (modify ?nqns (next_qns 6))
              )
            )
          )
        )
      )
    )
  )
)


(defrule conclusion_compile
  (declare (salience -70))
  ?con1 <- (conclusion (laptop ?id1) (rank ?rank) (cf ?cf1))
  ?con2 <- (conclusion (laptop ?id2) (rank ?rank) (cf ?cf2))
  (test (neq ?con1 ?con2))
  =>
  (if (< ?cf1 ?cf2) then
    (if (< ?rank 3) then
      (modify ?con1 (rank (+ ?rank 1)))
      else
      (retract ?con1)
    )
    else
    (if (< ?rank 3) then
      (modify ?con2 (rank (+ ?rank 1)))
      else
      (retract ?con2)
    )
  )
)

(defrule conclusion_push
  (declare (salience -75))
  ?nqns <- (next_question (next_qns "C"))
  ?product <- (product (eval 0) (id ?id) (cf ?cf))
  =>
  (modify ?product (eval 2))
  (assert (conclusion (laptop ?id) (rank 1) (cf ?cf)))
)

;; reset all factor_hit_eval
(defrule reset_factor_eval
  (declare (salience -80))
  ?factor_hit <- (factor_hit (eval ?eval&:(eq ?eval 1)))
  =>
  (modify ?factor_hit (eval 0))
)
(defrule reset_product_eval
  (declare (salience -90))
  ?p <- (product (eval 0))
  =>
  (modify ?p (eval 2))
)
