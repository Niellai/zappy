;; All slot stores cf range values except for name
;; cfs from current goal will try to match within product cf range.
(deftemplate product_factor
    (slot id)
    (slot factor)
    (slot match)
    (slot eval)
    (slot min)
    (slot max)
)

(deftemplate product
    (slot id)
    (slot name)
    (slot code)
    (slot eval)
    (slot cf)
)

(deftemplate product_working_cf
    (slot id)
    (slot cf)
)

(deftemplate current_goal
  (slot factor)
  (slot value)
)

(deftemplate working_goal
  (slot factor)
  (slot value)
)

(deftemplate factor_hit
  (slot factor)
  (slot count)
  (slot eval)
  (slot add_to_total)
)

(deftemplate total_factor_hit
  (slot count)
)

(deftemplate question_count
  (slot count)
  (multislot question)
  (multislot answer)
)

(deftemplate factor_weight
  (slot factor)
  (slot weight)
)

;; question and answer goes in pairs
(deftemplate  question (slot id) (slot qns))
(deftemplate answer (slot id) (multislot ans_id) (multislot ans))
;; store all index of questions and answers
(deftemplate user_answer  (slot qns_id) (slot user_ans))
(deftemplate cur_question (slot cur_qns) (slot cur_ans))
(deftemplate next_question (slot next_qns))
(deftemplate conclusion (slot laptop) (slot rank) (slot cf))
