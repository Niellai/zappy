-1 ~ -0.3 Low
-0.3 ~ 0.3 Medium
0.3 ~ 1 High

-1 ~ 0 No
0 ~ 1 Yes

deffact (evaluation point 0.7) (max_qns 7)

(current_goal (weight 0.5) (.....)

product (multislot weight ) (min -0.3) (max 0.3) (match -1) 

factor_cf_rank (weight 0.9) (screen_size 0.85) (battery 0.8)

Are you a student?
Yes (Weight/Battery)
	assert (working_goal)
	working_goal (weight -0.9) (battery 0.2) (screen_size 0)
	(current goal (weight -0.6) (battery 0.4) (......)

Check Matches
Check match Rule (foreach product) (hp envy)
	(current goal (weight >  min) ( weight < max)
	assert product(match 1)
	
Check mismatch Rule (foreach product) (hp envy)
	(current goal (weight <  min)  or ( weight > max)
	assert product(match -1)
	
Compilation Rule (foreach product) (hp envy)
	=>
	(sum (product 
		weight (match 1)* factor_cf_rank(weight) ==> 0.9
		screen_size (match -1)* factor_cf_rank(screen_size) == -0.85
	...) / 11
	==> 0.64 (total_cf)
	
Concluding Rule (foreach product)
	total_cf > evaluation point or number of qns > max_qns
	==> recommend product, product
	